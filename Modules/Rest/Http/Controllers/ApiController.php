<?php

namespace Modules\Rest\Http\Controllers\API;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Modules\Rest\Exceptions\ApiException;
use Modules\Rest\Helpers\ResponseBuilder;
use Modules\Rest\Http\Requests\ApiRequest;

class ApiController extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $modelClass = null;
    
    //list methods allow to call
    protected $alowMethods = [
        'store',
        'show',
        'update',
        'destroy'];
    protected $perPage     = 200;
    
    //when call api, these fields must have in input
    protected $requireFieldNames = [];
    
    //array to store field name and value from requireFieldNames, can add more something
    protected $searchFields = [];
    
    //array to store field name and value of 'no-change' fields when update new record such as id, user_id, etc...
    protected $staticAttribs = [];
    
    protected $collections = [];
    
    protected $route;
    
    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    
    public function __construct() {
        $this->route = Route::current();
    }
    
    protected function index($request) {
        $this->checkMethod('index');
        $this->fillRequireData($request);
        $itemPerPage = $request->input('per_page', $this->perPage);
        $dataWithPaginate = $this->modelClass::query()->with($this->collections)->where($this->searchFields)->paginate($itemPerPage)->toArray();
        $data = $dataWithPaginate['data'];
        unset($dataWithPaginate['data']);
        return ResponseBuilder::Success([
            'paging' => $dataWithPaginate,
            'data'   => $data,
        ]);
    }
    
    /**
     * @param ApiRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    protected function store(ApiRequest $request) {
        $this->checkMethod('store');
        $item = new $this->modelClass();
        $item->fill($request->all());
        $item = $this->setStaticAttribs($item);
        
        if (in_array('password', $item->getFillable())) $item->password = Hash::make($item->password);
        try {
            $item->save();
        } catch (\Exception $e) {
            return ResponseBuilder::Fail('can_not_add_item');
        }
        return ResponseBuilder::success($item);
    }
    
    /**
     * @param ApiRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    protected function show(ApiRequest $request) {
        $id = $this->route->parameter($this->route->parameterNames[sizeof($this->route->parameterNames) - 1]);
        $this->checkMethod('show');
        $this->fillRequireData($request);
        $item = $this->modelClass::query()->where($this->searchFields)->find($id);
        if ($item) return ResponseBuilder::success($item);
        
        return ResponseBuilder::Fail('invalid_item');
    }
    
    /**
     * @param ApiRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    protected function update(ApiRequest $request) {
        $id = $this->route->parameter($this->route->parameterNames[sizeof($this->route->parameterNames) - 1]);
        $this->checkMethod('update');
        $this->fillRequireData($request);
        $item = $this->modelClass::query()->where($this->searchFields)->find($id);
        if ($item === null) return ResponseBuilder::Fail('item_not_found');
        $item->fill($request->all());
        $item->id = $id;
        $item = $this->setStaticAttribs($item);
        
        if (in_array('password', $item->getFillable()) && $request->input('password', false))
            $item->password = Hash::make($item->password);
        
        try {
            $item->save();
        } catch (\Exception $e) {
            return ResponseBuilder::Fail('can_not_update_item');
        }
        return ResponseBuilder::success($item);
    }
    
    /**
     * @param ApiRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    protected function destroy(ApiRequest $request) {
        $id = $this->route->parameter($this->route->parameterNames[sizeof($this->route->parameterNames) - 1]);
        $this->checkMethod('destroy');
        $this->fillRequireData($request);
        $item = $this->modelClass::query()->where($this->searchFields)->find($id);
        if ($item === null) return ResponseBuilder::Fail('item_not_found');
        try {
            $item->delete();
        } catch (\Exception $e) {
            return ResponseBuilder::Fail('can_not_delete_item');
        }
        return ResponseBuilder::success();
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    protected function create() {
        return ResponseBuilder::Fail('invalid_request');
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    protected function edit() {
        return ResponseBuilder::Fail('invalid_request');
    }
    
    /**
     * @param $methodName
     * @return bool
     * @throws ApiException
     */
    private function checkMethod($methodName) {
        if (!$this->modelClass) return false;
        if (in_array($methodName, $this->alowMethods) === false) return false;
        throw new ApiException('invalid_request');
    }
    
    /**
     * @param $request
     * @throws ApiException
     */
    private function fillRequireData(&$request) {
        foreach ($this->requireFieldNames as $fieldName) {
            if (!isset($this->searchFields[$fieldName])) {
                $fieldVal = $request->input($fieldName, null);
                if ($fieldVal === null) $fieldVal = $this->route->parameter($fieldName);
                if ($fieldVal === null) throw new ApiException('missing_input');
                $this->searchFields[$fieldName] = $fieldVal;
            }
        }
    }
    
    /**
     * @param $item
     * @return mixed
     */
    private function setStaticAttribs($item) {
        foreach ($this->staticAttribs as $key => $value)
            if (in_array($key, $item->getFillable()))
                $item->$key = $value;
        return $item;
    }
}
