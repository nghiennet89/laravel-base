<?php
/**
 * Created by IntelliJ IDEA.
 * User: nghiennet89
 * Date: 7/9/2019
 * Time: 9:52 AM
 */

namespace Modules\Rest\Helpers;


class Utils {
    /*
     * Cast object to another class
     */
    public static function CastTo($object, $newClass) {
        $newObj = new $newClass;
        foreach (get_object_vars($object) as $key => $name) {
            $newObj->$key = $name;
        }
        return $newObj;
    }
}
