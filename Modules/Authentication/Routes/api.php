<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'authentication',
    'as'     => 'authentication.',
], function () {
    Route::group(['namespace' => 'JWTAuth'], function () {
        Route::group([
            'prefix' => 'admin',
            'as'     => 'admin.'], function () {
            Route::post('login', 'Admin@login')->name('login');
            Route::post('reset-password', 'Admin@resetPassword')->name('resetPassword');
            Route::group(['middleware' => 'auth:api-admin'], function () {
                Route::get('info', 'Admin@info')->name('info');
                Route::post('logout', 'Admin@logout')->name('logout');
                Route::post('change-password', 'Admin@changePassword')->name('changePassword');
            });
        });
    });
    Route::group(['namespace' => 'JWTAuth'], function () {
        Route::group([
            'prefix' => 'user',
            'as'     => 'user.'], function () {
            Route::post('login', 'User@login')->name('login');
            Route::post('reset-password', 'Admin@resetPassword')->name('resetPassword');
            Route::group(['middleware' => 'auth:api-user'], function () {
                Route::post('logout', 'User@logout')->name('logout');
                Route::post('change-password', 'User@changePassword')->name('changePassword');
            });
        });
    });
    Route::group([
        'namespace'  => 'Admin',
        'prefix'     => 'admin',
        'middleware' => 'auth:api-admin',
    ], function () {
        Route::resources([
            'user'                  => 'UserController',
            'role'                  => 'RoleController',
            'route'                 => 'RouteController',
            'route/{routeId}/roles' => 'RouteRolesController',
            'user/{userId}/roles'   => 'UserRolesController',
        ]);
    });
});
