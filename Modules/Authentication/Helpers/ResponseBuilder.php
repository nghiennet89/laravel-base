<?php

namespace Modules\Authentication\Helpers;

class ResponseBuilder {
    const FAIL    = 0;
    const SUCCESS = 1;

    public static function Success($data = null, $message = '') {
        $res = [
            'status'  => self::SUCCESS,
            'code'    => null,
            'message' => $message,
            'data'    => $data,
        ];
        return response()->json($res);
    }

    public static function Fail($message = '', $data = null, $errorCode = 400) {
        $res = [
            'status'  => self::FAIL,
            'code'    => $errorCode,
            'message' => $message,
            'data'    => $data,
        ];
        if ($errorCode) return response()->json($res, $errorCode);
        return response()->json($res);
    }

    public static function HandleException(\Exception $exception) {
        $res = [
            'status'  => self::FAIL,
            'code'    => $exception->getCode(),
            'message' => $exception->getMessage(),
            'data'    => null,
        ];
        return response()->json($res);
    }

    public static function build($status = null, $errorCode = null, $message = '', $data = null) {
        $res = [
            'status'  => $status,
            'code'    => $errorCode,
            'message' => $message,
            'data'    => $data,
        ];
        if ($errorCode) return response()->json($res, $errorCode);
        return response()->json($res);
    }

    public static function plainText($data) {
        if (!is_string($data)) $data = json_encode($data);
        return response($data, 200)->header('Content-Type', 'text/plain');
    }
}
