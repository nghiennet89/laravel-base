<?php

namespace Modules\Authentication\Providers;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Authentication\Exceptions\Handler;
use Symfony\Component\Finder\Finder;

class AuthenticationServiceProvider extends ServiceProvider {
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot() {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->registerCommands('Modules\Authentication\Console');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations() {
        $langPath = resource_path('lang/modules/authentication');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'authentication');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'authentication');
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig() {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('authentication.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'authentication'
        );

        $configFiles = glob(__DIR__ . '/../../../config/*.php');
        foreach ($configFiles as $configFile) {
            $fileName = basename($configFile);
            $configName = str_replace('.php', '', $fileName);
            $moduleConfigPath = __DIR__ . '/../Config/' . $configName . '.php';
            if (file_exists($moduleConfigPath)) {
                $config = require $moduleConfigPath;
                if ($config) $this->app['config']->set($configName, array_replace_recursive($this->app['config']->get($configName), $config));
            }
        }
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews() {
        $viewPath = resource_path('views/modules/authentication');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath,
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/authentication';
        }, \Config::get('view.paths')), [$sourcePath]), 'authentication');
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories() {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    protected function registerCommands($namespace = '') {
        $finder = new Finder();
        $finder->files()->name('*.php')->in(__DIR__ . '/../Console');
        $classes = [];
        foreach ($finder as $file) {
            $class = $namespace . '\\' . $file->getBasename('.php');
            array_push($classes, $class);
        }
        $this->commands($classes);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->register(RouteServiceProvider::class);
        $this->app->singleton(
            ExceptionHandler::class,
            Handler::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return [];
    }
}
