<?php

namespace Modules\Authentication\Models;

use Illuminate\Database\Eloquent\Model;

class RouteRoles extends Model {
    protected $table = "route_roles";

    protected $fillable = [
        'id',
        'role_id',
        'route_id',
    ];
}
