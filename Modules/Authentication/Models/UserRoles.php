<?php

namespace Modules\Authentication\Models;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model {
    protected $table = "user_roles";
    
    protected $fillable = [
        'id',
        'user_id',
        'role_id',
    ];
    
    public function role() {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
