<?php

namespace Modules\Authentication\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject {
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'username',
        'email',
        'password',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims() {
        return [
            'user' => [
                'id' => $this->getKey(),
            ],
        ];
    }
    
    public function delete() {
        DB::beginTransaction();
        $deleteMe = false;
        try {
            UserRoles::query()->where('user_id', '=', $this->id)->delete();
            $deleteMe = parent::delete();
        } catch (\Exception $e) {
            DB::rollBack();
        }
        DB::commit();
        return $deleteMe;
    }
    
    public function user_roles() {
        return $this->hasMany(UserRoles::class, 'user_id');
    }
}
