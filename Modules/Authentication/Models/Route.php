<?php

namespace Modules\Authentication\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Route extends Model {

    protected $fillable = [
        'id',
        'method',
        'uri',
        'name',
        'controller',
        'middleware',
    ];

    public function delete() {
        DB::beginTransaction();
        $deleteMe = false;
        try {
            RouteRoles::query()->where('route_id', '=', $this->id)->delete();
            $deleteMe = parent::delete();
        } catch (\Exception $e) {
            DB::rollBack();
        }
        DB::commit();
        return $deleteMe;
    }
}
