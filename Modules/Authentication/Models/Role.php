<?php

namespace Modules\Authentication\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model {
    protected $table = "roles";

    protected $fillable = [
        'id',
        'name',
        'description',
    ];

    public function delete() {
        DB::beginTransaction();
        $deleteMe = false;
        try {
            UserRoles::query()->where('role_id', '=', $this->id)->delete();
            RouteRoles::query()->where('role_id', '=', $this->id)->delete();
            $deleteMe = parent::delete();
        } catch (\Exception $e) {
            DB::rollBack();
        }
        DB::commit();
        return $deleteMe;
    }
}
