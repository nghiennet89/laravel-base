<?php

namespace Modules\Authentication\Console;

use Illuminate\Console\Command;
use Modules\Authentication\Models\Route;

class DumpRouteRoles extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'route:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $allRoute = \Illuminate\Support\Facades\Route::getRoutes()->get();
        foreach ($allRoute as $route) {
            $action = $route->getAction();
            $routeData = [
                'method'     => (sizeof($route->methods) > 1) ? implode('|', $route->methods) : $route->methods[0],
                'uri'        => $route->uri,
                'name'       => $route->getName(),
                'controller' => isset($action['controller']) ? $action['controller'] : 'Closure',
                'middleware' => (sizeof($action['middleware']) > 1) ? implode('|', $action['middleware']) : $action['middleware'][0],
            ];
            $routeItem = Route::query()
                ->where('method', $routeData['method'])
                ->where('uri', $routeData['uri'])
                ->where('name', $routeData['name'])
                ->first();
            if (!$routeItem) {
                $routeItem = new Route();
                $routeItem->fill($routeData);
            } else {
                $routeItem->controller = $routeData['controller'];
                $routeItem->middleware = $routeData['middleware'];
            }
            $routeItem->save();
            echo $routeItem->uri . "\n";
        }
    }
}
