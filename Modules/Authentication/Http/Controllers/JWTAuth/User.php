<?php

namespace Modules\Authentication\Http\Controllers\JWTAuth;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Modules\Authentication\Helpers\ResponseBuilder;

class User extends Controller {
    use AuthenticatesUsers;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function login(Request $request) {
        $token = null;
        try {
            $cre = $this->credentials($request);
            $guard = $this->guard();
            if (!$token = $guard->attempt($cre)) {
                return ResponseBuilder::Fail('auth_fail', null, 422);
            }
        } catch (\Exception $e) {
            return ResponseBuilder::Fail('failed_to_create_token', null, 500);
        }
        $loginData = [
            'token' => $token,
            'user'  => $guard->user(),
        ];
        return ResponseBuilder::Success($loginData, 'login_success');
    }

    protected function guard() {
        return auth('api-user');
    }

    protected function username() {
        return 'username';
    }
}
