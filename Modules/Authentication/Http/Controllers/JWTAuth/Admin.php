<?php

namespace Modules\Authentication\Http\Controllers\JWTAuth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Authentication\Helpers\ResponseBuilder;

class Admin extends Controller {
    use AuthenticatesUsers;
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    public function login(Request $request) {
        $token = null;
        try {
            $cre = $this->credentials($request);
            $guard = $this->guard();
            if (!$token = $guard->attempt($cre)) {
                return ResponseBuilder::Fail('auth_fail', null, 422);
            }
        } catch (\Exception $e) {
            return ResponseBuilder::Fail('failed_to_create_token', $e, 500);
        }
        $loginData = [
            'token' => $token,
            'user'  => $guard->user(),
        ];
        return ResponseBuilder::Success($loginData, 'login_success');
    }
    
    public function info() {
        return ResponseBuilder::Success($this->guard()->user());
    }
    
    public function resetPassword(Request $request) {
    
    }
    
    public function changePassword(Request $request) {
        $adminId = $request->input('id');
        $admin = \Modules\Authentication\Models\Admin::query()->find($adminId);
        if (!$admin) return ResponseBuilder::Fail('user_not_found');
        $oldPassword = $request->input('password');
        if (!Hash::check($oldPassword, $admin->password)) return ResponseBuilder::Fail('invalid_password');
        $newPassword = $request->input('password-new');
        $admin->password = Hash::make($newPassword);
        $admin->save();
        return ResponseBuilder::Success($admin);
    }
    
    protected function guard() {
        return auth('api-admin');
    }
    
    protected function username() {
        return 'username';
    }
}
