<?php

namespace Modules\Authentication\Http\Controllers\Admin;


use Modules\Authentication\Models\Route;

class RouteController extends Controller {
    public function __construct() {
        $this->modelClass = Route::class;
        $this->alowMethods = [
            'index',
            'show'];
        return parent::__construct();
    }
}
