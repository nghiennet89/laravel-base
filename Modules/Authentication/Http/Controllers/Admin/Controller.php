<?php

namespace Modules\Authentication\Http\Controllers\Admin;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Modules\Authentication\Helpers\ResponseBuilder;
use Modules\Rest\Http\Controllers\API\ApiController;

class Controller extends ApiController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, AuthenticatesUsers;
    protected $user;

    public function __construct() {
        $this->user = auth('api-admin')->user();
        if (!$this->user) return ResponseBuilder::Fail('Invalid User');
    }
}
