<?php

namespace Modules\Authentication\Http\Controllers\Admin;


use Modules\Authentication\Models\Role;

class RoleController extends Controller {
    public function __construct() {
        $this->modelClass = Role::class;
        return parent::__construct();
    }
}
