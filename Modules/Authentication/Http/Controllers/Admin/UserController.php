<?php

namespace Modules\Authentication\Http\Controllers\Admin;


use Modules\Authentication\Models\User;

class UserController extends Controller {
    public function __construct() {
        $this->modelClass = User::class;
        return parent::__construct();
    }
}
