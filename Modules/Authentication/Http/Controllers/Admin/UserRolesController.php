<?php

namespace Modules\Authentication\Http\Controllers\Admin;


use Modules\Authentication\Models\UserRoles;

class UserRolesController extends Controller {
    public function __construct() {
        $this->modelClass = UserRoles::class;
        return parent::__construct();
    }
}
