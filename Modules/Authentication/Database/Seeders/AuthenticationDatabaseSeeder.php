<?php

namespace Modules\Authentication\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Authentication\Models\Admin;
use Modules\Authentication\Models\User;

class AuthenticationDatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();
        DB::table('admin')->truncate();
        Admin::query()->firstOrCreate([
            'email'    => 'nghiennet89@gmail.com',
            'username' => 'admin',
            'password' => Hash::make('admin@123'),
        ]);
    
        DB::table('users')->truncate();
        User::query()->firstOrCreate([
            'email'    => 'user@test.com',
            'username' => 'test_user',
            'password' => Hash::make('test_user@123'),
        ]);
    }
}
