<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RouteTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('routes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('method');
            $table->string('uri')->unique();
            $table->string('name')->unique();
            $table->string('controller')->nullable();
            $table->string('middleware')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('routes');
    }
}
