<?php

return [
    'guards'    => [
        'api-admin' => [
            'driver'   => 'jwt',
            'provider' => 'admin',
        ],
        'api-user'  => [
            'driver'   => 'jwt',
            'provider' => 'users',
        ],
    ],
    'providers' => [
        'admin' => [
            'driver' => 'eloquent',
            'model'  => Modules\Authentication\Models\Admin::class,
        ],
        'users' => [
            'driver' => 'eloquent',
            'model'  => Modules\Authentication\Models\User::class,
        ],
    ],
];
