<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Vue Material Admin Template</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700%7CMaterial+Icons' rel="stylesheet">
    <link href='./static/styles/app.css' rel="stylesheet">
    <script src="./static/scripts/echarts-en.min.js"></script>
    <script src="./static/scripts/jquery-1.12.4.min.js"></script>
    <script src="./static/scripts/app.js"></script>
    <link rel="shortcut icon" href="/static/m.png" type="image/x-icon">
    <meta name="description" content="Vue Material Admin Template custom by nghiennet89 - email nghiennet89@gmail.com">
    <meta name="keywords" content="nghiennet89">
    <link href="/0.js" rel="prefetch">
    <link href="/1.js" rel="prefetch">
    <link href="/routes.js" rel="prefetch">
    <link href="/vendors~routes.js" rel="prefetch">
    <link href="/app.js" rel="preload" as="script">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/icons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="theme-color" content="#4DBA87">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="vma">
    <link rel="apple-touch-icon" href="/img/icons/apple-touch-icon-152x152.png">
    <link rel="mask-icon" href="/img/icons/safari-pinned-tab.svg" color="#4DBA87">
    <meta name="msapplication-TileImage" content="/img/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#000000">
</head>

<body>
<noscript>
    <strong>We're sorry but our system doesn't work properly without JavaScript enabled. Please enable it to
        continue.</strong>
    <strong>Chúng tôi xin lỗi nhưng hệ thống của chúng tôi không hoạt động khi không bật Javascript. Hãy mở lại nó để
        tiếp tục sử dụng.</strong>
</noscript>
<div id="app"></div>
<!-- built files will be auto injected -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3881136-11"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag () {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-3881136-11');
</script>

<script type="text/javascript" src="/app.js"></script>
</body>
</html>
