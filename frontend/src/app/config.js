export default {
  appName: 'app',
  baseUrl: 'http://ets-base.local',
  apiUrl: 'http://ets-base.local/api',
  auth: {
    loginData: {url: 'authentication/admin/login', method: 'POST', redirect: '/dashboard', fetchUser: true},
    fetchData: {url: 'authentication/admin/info', method: 'GET', enabled: true},
    refreshData: {url: 'authentication/admin/info', method: 'GET', enabled: true, interval: 30},
    logoutData: {url: 'authentication/admin/logout', method: 'POST', redirect: '/login', makeRequest: false},
  }
}
