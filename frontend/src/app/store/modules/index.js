import users from './users';
import roles from './roles';
import menu from './menu';

export default {
  users,
  roles,
  menu
}
