const namespaced = true;
const state = {};
const mutations = {};
const getters = {};
const actions = {};
export default {
  namespaced,
  state,
  mutations,
  getters,
  actions
}
