import Vue from "vue";
import Vuex from "vuex";
import modules from './modules'

Vue.use(Vuex);

const state = {};
const getters = {};
const mutations = {};
const actions = {};

const store = new Vuex.Store({
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions,
  modules
});

Vue.store = store;

export default store;
