import Vue from "vue";
import VueResource from 'vue-resource';
import VueAuth from '@websanova/vue-auth';
import config from "../config";

Vue.use(VueResource);
Vue.http.options.root = config.apiUrl;
Vue.use(VueAuth, {
  auth: {
    request: (req, token) => {
      req.params = req.params || {};
      req.params.token = token;
      return req;
    },
    response: (res) => {
      let token = (res.body && res.body.data && res.body.data.token) ? res.body.data.token : null;
      if (token) return token.trim();
    }
  },
  http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  ...config.auth
});
const _Vue = Vue;

//handle token expired or api error
Vue.http.interceptors.push((req, next) => {
  next((response) => {
    if (response.status === 401 && ['UnauthorizedAccess', 'invalid_token', 'InvalidToken'].indexOf(response.body.message) > -1) {
      _Vue.auth.logout();
    } else if (response.status === 500) {
      _Vue.router.push({name: '500'});
    }
  })
});
