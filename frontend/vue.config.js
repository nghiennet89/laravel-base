const path = require('path');

module.exports = {
  // proxy API requests to Valet during development
  
  /*devServer: {
    proxy: 'http://laracon.test'
  },*/
  
  // output built static files to Laravel's public dir.
  // note the "build" script in package.json needs to be modified as well.
  outputDir: '../public',
  
  // modify the location of the generated HTML file.
  // make sure to do this only in production.
  
  /*indexPath: process.env.NODE_ENV === 'production'
    ? '../resources/views/index.blade.php'
    : 'index.html',*/
  filenameHashing: false,
  chainWebpack: config => {
    config.resolve.alias.set('@', path.resolve(__dirname, 'src/lib'));
    config.resolve.alias.set('__app', path.resolve(__dirname, 'src/app'));
  },
  pages: {
    app: {
      entry: 'src/app/main.js',
      filename: 'index.html'
    },
    original: {
      entry: 'src/original/main.js',
      filename: 'original.html'
    }
  }
}
